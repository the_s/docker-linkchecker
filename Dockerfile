FROM registry.gitlab.com/pages/hugo/hugo_extended:0.78.2

# Install linkchecker
# https://github.com/gjtorikian/html-proofer
RUN set -eux && apk update && apk upgrade && apk add --no-cache \
bash gcc git libc-dev libcurl ruby-dev zlib-dev ruby ruby-bundler \
build-base libxml2 libxml2-dev libxslt libxslt-dev ruby-rdoc \
ruby-bigdecimal
RUN gem install html-proofer

EXPOSE 1313
